import React, { useState, useEffect } from 'react'

const QuizzOver = React.forwardRef((props, ref) => {
    const [asked, setAsked] = useState([])

    useEffect(() => {
        setAsked(ref.current)
    }, [ref])

    const questionsAnwser = asked.map(question => {
        return <tr key={question.id}>
            <td>{question.question}</td>
            <td>{question.answer}</td>
            <td>
                <button className="btnInfo">Infos</button>
            </td>
        </tr>
    })
    return (
        <>
            <div className="stepsBtnContainer">
                <p className="successMsg">Bravo, vous êtes le meilleur !</p>
                <button className="btnResult success">Niveau suivant</button>
            </div>
            <div className="percentage">
                <div className="progressPercent">Réussite : 10%</div>
                <div className="progressPercent">Note : 10/10</div>
            </div>
            <hr />
            <p>Les réponses aux questions posées : </p> 
            <div className="answerContainer">
                <table className="answers">
                    <thead>
                        <tr>
                            <td>Questions</td>
                            <td>Réponses</td>
                            <td>Infos</td>
                        </tr>
                    </thead>
                    <tbody>
                        {questionsAnwser}
                    </tbody>
                </table>
            </div>
        </>
    )
})


export default React.memo(QuizzOver)
