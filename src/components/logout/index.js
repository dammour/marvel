import React, { useState, useEffect, useContext } from 'react'
import { FirebaseContext } from '../firebase'

const styleDeconnexion = {
    padding: '0.5rem 1.2rem'
}

const Logout = () => {
    const [checked, setChecked] = useState(false);

    const firebase = useContext(FirebaseContext)

    const handleChecked = e => {
        setChecked(e.target.value);
    }

    useEffect(() => {
        if(checked) {
            firebase.logOut()
        }
    }, [checked, firebase])

    return (
        <div className="logoutContainer">
               <label style={styleDeconnexion}>Se déconnecter</label>
               <label className="switch">
                <input type="checkbox" checked={checked} onChange={handleChecked} />
                <span className="slider round"></span>
           </label>
        </div>
    )
}

export default Logout
