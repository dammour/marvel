import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { FirebaseContext } from '../firebase'

const ForgetPassword = (props) => {
    const [email, setEmail] = useState('')
    const [success, setSuccess] = useState(null)
    const [error, setError] = useState(null)

    const firebase = useContext(FirebaseContext)

    const handleSubmit = e => {
        e.preventDefault();
        firebase.resetPassword(email)
            .then(() => {
                setError(null)
                setSuccess(`Un email vient de vous être envoyé sur ${email} pour réinitialiser votre mot de passe`)
                setEmail('')

                setTimeout(() => {
                    props.history.push('/login')
                }, 5000)
            })
            .catch(error => {
                setError(error)
                setEmail('')
            })
    }

    const disabled = email === ''

    const successMessage = {
        border: '1px solid green',
        backgroundColor: 'green',
        color: '#ffffff'
    }

    return (
        <div className="signUpLoginBox">
            <div className="slContainer">
                <div className="formBoxLeftForget"></div>
                    <div className="formBoxRight">
                        <div className="formContent">
                            { success && <span style={successMessage}>{success}</span>}
                            { error && <span>{error.message}</span>}
                            <h2>Mot de passe oublié</h2>
                            <form action="" onSubmit={handleSubmit}>
                                <div className="inputBox">
                                    <input type="email" value={email} onChange={e => setEmail(e.target.value)} required />
                                    <label htmlFor="email">Email</label>
                                </div>
                                <button disabled={disabled}>Récupérer</button>
                            </form>
                            <div className="linkContainer">
                                <Link to="/login" className="simpleLink">Déjà inscrit ? Connectez-vous</Link>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    )
}

export default ForgetPassword
