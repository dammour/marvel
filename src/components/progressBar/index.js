import React from 'react'

const ProgressBar = ({ idQuestion, maxQuestions }) => {
    const actualQuestion = idQuestion + 1

    const getPercent = (totalQuestion, questionId) => {
        return (100/totalQuestion) * questionId
    }

    const progressPercent = getPercent(maxQuestions, actualQuestion)
    
    const progression = {
        width: `${progressPercent}%`
    }

    return (
        <>
            <div className="percentage">
                <div className="progressPercent">
                    Question : {`${idQuestion + 1}/${maxQuestions}`}
                </div>
                <div className="progressPercent">
                    Progression : {`${progressPercent}%`}
                </div>
            </div>
            <div className="progressBar">
                <div className="progressBarChange" style={progression}></div>
            </div>
        </>
    )
}

export default React.memo(ProgressBar)
