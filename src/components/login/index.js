import React, { useState, useEffect, useContext } from 'react'
import { Link } from 'react-router-dom'
import { FirebaseContext } from '../firebase'

const Login = (props) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [btn, setBtn] = useState(false)
    const [error, setError] = useState('')

    const firebase = useContext(FirebaseContext)

    useEffect(() => {
        if(password.length > 5 && email !== '') {
            setBtn(true)
        } else if(btn) {
            setBtn(false)
        }
    }, [password, email, btn])

    const handleSubmit = e => {
        e.preventDefault();
        firebase.loginUser(email, password)
            .then((user) => {
                setEmail('')
                setPassword('')
                props.history.push('/welcome')
            })
            .catch(error => {
                setEmail('')
                setPassword('')
                setError(error);
            })
    }

    return (
        <div className="signUpLoginBox">
            <div className="slContainer">
                <div className="formBoxLeftLogin"></div>
                    <div className="formBoxRight">
                        <div className="formContent">
                            {error !== '' && <span>{error.message}</span>}
                            <h2>Connexion</h2>
                            <form action="" onSubmit={handleSubmit}>
                                <div className="inputBox">
                                    <input type="text" value={email} onChange={e => setEmail(e.target.value)} required autoComplete="off" />
                                    <label htmlFor="email">Email</label>
                                </div>
                                <div className="inputBox">
                                    <input type="password" value={password} onChange={e => setPassword(e.target.value)} required autoComplete="off" />
                                    <label htmlFor="password">Password</label>
                                </div>
                                {btn ? <button >Connexion</button> : <button disabled>Connexion</button>}
                            </form>
                            <div className="linkContainer">
                                <Link to="/signup" className="simpleLink">Nouveau sur Marvel Quizz ? Inscrivez-vous</Link>
                                <br />
                                <Link to="/forgetpassword" className="simpleLink">Mot de passe oublié ?</Link>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    )
}

export default Login
