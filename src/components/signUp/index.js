import React, { useState, useContext } from 'react'
import { Link } from 'react-router-dom'
import { FirebaseContext } from '../firebase'

const SignUp = (props) => {
    const data = {
        pseudo: '',
        email: '',
        password: '',
        confirmPassword: ''
    }

    const [login, setLogin] = useState(data)
    const [error, setError] = useState('')

    const firebase = useContext(FirebaseContext)

    const handleChange = e => {
        setLogin({...login, [e.target.id]: e.target.value})
    }
    
    const handleSubmit = e => {
        e.preventDefault()
        const { email, password, pseudo } = login
        firebase.signUpUser(email, password)
            .then((authUser) => {
                return firebase.user(authUser.user.uid).set({
                    pseudo,
                    email
                })
            })
            .then(() => 
                setLogin({...data}),
                props.history.push('/welcome')
            )
            .catch(error => {
                console.log(error)
                setError(error)
                setLogin({...data})
            })
    }
    const { pseudo, email, password, confirmPassword } = login

    const btn = pseudo === '' || email === '' || password === '' || password !== confirmPassword ? <button disabled>Inscription</button> : <button>Inscription</button>

    const errorMsg = error !== '' && <span>{error.message}</span>

    return (
        <div className="signUpLoginBox">
            <div className="slContainer">
                <div className="formBoxLeftSignup"></div>
                    <div className="formBoxRight">
                        <div className="formContent">
                            {errorMsg}
                            <h2>Inscription</h2>
                            <form action="" onSubmit={handleSubmit}>
                                <div className="inputBox">
                                    <input type="text" id="pseudo" value={pseudo} required autoComplete="off" onChange={handleChange} />
                                    <label htmlFor="pseudo">Pseudo</label>
                                </div>
                                <div className="inputBox">
                                    <input type="email" id="email" value={email} required autoComplete="off" onChange={handleChange} />
                                    <label htmlFor="email">Email</label>
                                </div>
                                <div className="inputBox">
                                    <input type="password" id="password" value={password} required autoComplete="off" onChange={handleChange} />
                                    <label htmlFor="password">Password</label>
                                </div>
                                <div className="inputBox">
                                    <input type="password" id="confirmPassword" value={confirmPassword} required autoComplete="off" onChange={handleChange} />
                                    <label htmlFor="confirmPassword">Confirmation Password</label>
                                </div>
                                {btn}
                            </form>
                            <div className="linkContainer">
                                <Link to="/login" className="simpleLink">Déjà inscrit ? Connectez-vous</Link>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    )
}

export default SignUp
