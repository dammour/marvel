import app from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyAQZaau2VAJufle42NJU70d8fDtvJmZWsA",
    authDomain: "marvelquizz-e4af3.firebaseapp.com",
    projectId: "marvelquizz-e4af3",
    storageBucket: "marvelquizz-e4af3.appspot.com",
    messagingSenderId: "784638134470",
    appId: "1:784638134470:web:3fb20f426b62f5d3daafc7"
};

class Firebase {
    constructor() {
        app.initializeApp(config)
        this.auth = app.auth()
        this.db = app.firestore()
    }

    // signUp
    signUpUser = (email, password) => this.auth.createUserWithEmailAndPassword(email, password)
    

    // login
    loginUser = (email, password) => this.auth.signInWithEmailAndPassword(email, password)
    

    // logOut
    logOut = () => this.auth.signOut()

    // password reset
    resetPassword = (email) => this.auth.sendPasswordResetEmail(email)

    // db
    user = (uid) => this.db.doc(`users/${uid}`)
}

export default Firebase