import React, { useState, useEffect, useContext } from 'react'
import { FirebaseContext } from '../firebase'
import Logout from '../logout'
import Quizz from '../quizz'

const Welcome = (props) => {
    const [userSession, setUserSession] = useState(null)
    const [userData, setUserData] = useState(null)

    const firebase = useContext(FirebaseContext)

    useEffect(() => {
        let listener = firebase.auth.onAuthStateChanged(user => {
            user ? setUserSession(user) : props.history.push('/')
        })

        if(userSession !== null) {
            firebase.user(userSession.uid)
                .get()
                .then((doc) => {
                    if(doc && doc.exists) {
                        const data = doc.data()
                        setUserData(data)
                    }
                })
                .catch((e) => {
                    console.log(e);
                })
        }

        return () => {
            listener()
        }
    }, [userSession])

    return userSession === null ? 
        (<><div className="loader"></div><span></span></>) : 
        (<div className="quiz-bg">
            <div className="container">
                <Logout />
                <Quizz userData={userData} />
            </div>
        </div>)
}

export default Welcome
