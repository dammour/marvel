import React, { Component } from 'react'
import Levels from '../levels'
import ProgressBar from '../progressBar'
import { QuestionsQuizz } from '../questionsQuizz'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import QuizzOver from '../quizzOver'

toast.configure()

const stylePseudo = {
    margin: 'auto',
    paddingBottom: '2rem',
    fontSize: '1.5rem'
}

export default class Quizz extends Component {
    constructor(props) {
        super(props);
        this.state = {
            levelNames: ["debutant", "confirme", "expert"],
            quizzLevel: 0,
            maxQuestions: 10,
            storedQuestions: [],
            question: null,
            options: [], 
            idQuestion: 0,
            btnDisabled: true,
            userAnswer: null,
            score: 0,
            showWelcomeMsg: false,
            quizzEnd: false
        };
        this.showWelcomeMsg = this.showWelcomeMsg.bind(this)
    }

    storedDataRef = React.createRef();
    
    questions = quizz => {
        const fetchArrayQuizz = QuestionsQuizz[0].quizz[quizz]
        if(fetchArrayQuizz.length >= this.state.maxQuestions) {
            // permet le retrait des réponses (via dev tools react) dans l'Array 
            const newArray = fetchArrayQuizz.map(({ answer, ...otherInformations}) => otherInformations)
            this.setState({storedQuestions: newArray})
            this.storedDataRef.current = fetchArrayQuizz
        } else {
            console.log("Pas assez de questions");
        }
    }
    
    componentDidMount(prevProps) {
        this.questions(this.state.levelNames[this.state.quizzLevel])
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.state.storedQuestions !== prevState.storedQuestions) {
            this.setState({
                question: this.state.storedQuestions[this.state.idQuestion].question,
                options: this.state.storedQuestions[this.state.idQuestion].options,
            })
        }
        
        if(this.state.idQuestion !== prevState.idQuestion) {
            this.setState({
                question: this.state.storedQuestions[this.state.idQuestion].question,
                options: this.state.storedQuestions[this.state.idQuestion].options,
                userAnswer: null,
                btnDisabled: true
            })
        }

        if(this.props.userData !== null && this.props.userData.pseudo) {
            this.showWelcomeMsg(this.props.userData.pseudo)
        }
    }

    showWelcomeMsg = pseudo => {
        if(!this.state.showWelcomeMsg) {
            this.setState({
                showWelcomeMsg: true
            });
            toast.warn(`Bienvenue ${pseudo}, et bonne chance !`, {
                position: "top-right",
                autoClose: 4000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: false,
                progress: undefined,
            });
        }
    } 

    handleSubmitAnswer = selectedAnswer => {
        this.setState({ 
            userAnswer: selectedAnswer, 
            btnDisabled: false
        })
    }

    gameOver = () => {
        this.setState({
            quizzEnd: true
        })
    }

    nextQuestion = () => {
        if(this.state.idQuestion === this.state.maxQuestions - 1) {
            this.gameOver()
        } else {
            this.setState(prevState => ({
                idQuestion: prevState.idQuestion + 1
            }))
        }

    const goodAnwser = this.storedDataRef.current[this.state.idQuestion].answer

    if(this.state.userAnswer === goodAnwser) {
        this.setState(prevState => ({
            score: prevState.score + 1
        }))
        toast.success('🦄 Bravo ! + 1', {
            position: "top-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            });
        } else {
            toast.error('Raté ! 0', {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                });
        }
    }   

    render() {
        const displayQuestions = this.state.options.map((option, k) => {
            return (
                <p className={`answerOptions ${this.state.userAnswer === option ? "selected" : null}`} key={k} onClick={() => this.handleSubmitAnswer(option)}>{option}</p>
            )
        })

        let pseudo = this.props.userData !== null && this.props.userData.pseudo
        pseudo = pseudo.toString()

        const capitalizeFirstLetter = (string) => {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }

        const pseudoCapitalized = capitalizeFirstLetter(pseudo)

        return this.state.quizzEnd ? 
                    <QuizzOver ref={this.storedDataRef} /> : 
                <>
                    <div style={stylePseudo}>{this.props.userData !== null ? pseudoCapitalized : ''} est en train de répondre au Quizz !</div>
                    <div className="levelsContainer">
                        <h2 className="headingLevels">Débutant</h2>
                        <Levels />
                    </div>
                    <ProgressBar idQuestion={this.state.idQuestion} maxQuestions={this.state.maxQuestions} />
                    <h2>{this.state.question}</h2>

                    {displayQuestions}

                    <button className="btnSubmit" disabled={this.state.btnDisabled} onClick={() => this.nextQuestion()}>{this.state.idQuestion < this.state.maxQuestions - 1 ? "Suivant" : "Terminer"}</button>
                </>
    }
}