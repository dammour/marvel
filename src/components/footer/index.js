import React from 'react'

const Footer = () => {
    return (
        <footer className="">
            <div className="footer-container">
                <p>Projet réalisé par Dammour</p>
                <p>Les icônes Wolverine, Iron-man, Spiderman et Batman sont prises sur iconFinder.com</p>
            </div>
        </footer>
    )
}

export default Footer
