import React from 'react'
import { Switch, Route } from 'react-router-dom';
import Layout from '../layout';
import Welcome from '../welcome';
import Login from '../login';
import SignUp from '../signUp';
import ErrorPage from '../errorPage';
import ForgetPassword from '../forgetPassword';

const Router = () => {
    return (
        <Switch>
            <Route exact path="/" component={Layout} />
            <Route exact path="/welcome" component={Welcome} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={SignUp} />
            <Route exact path="/forgetpassword" component={ForgetPassword} />
            <Route component={ErrorPage} />
        </Switch>
    )
}

export default Router
