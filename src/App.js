import React from 'react';
import Header from './components/header';
import Router from './components/router';
import Footer from './components/footer';
import './App.css';

function App() {
  return (
    <>
      <Header />
      
      <Router />

      <Footer />
    </>
  );
}

export default App;
